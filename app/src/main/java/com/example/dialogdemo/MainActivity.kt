package com.example.dialogdemo

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener ,TimePickerDialog.OnTimeSetListener{
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var alertDialog=AlertDialog.Builder(this).setMessage("hello From Alert Dialog")
            .setTitle("Alert Dialog")
            .setCancelable(true)
            .create()


        var customDialog = Dialog(this)
        customDialog.setCancelable(true)
        customDialog.setContentView(R.layout.dialog_layout)

        var dpd=DatePickerDialog(this)
        dpd.setOnDateSetListener(this)

        var tpd=TimePickerDialog(this,this,12,0,false)



        var cd=Dialog(this)
        cd.setContentView(R.layout.custom_view)
        cd.setCancelable(true)
        cd.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.WRAP_CONTENT)
        cd.window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))

        alertdialog.setOnClickListener {
            alertDialog.show()
        }

        custom.setOnClickListener {
            customDialog.show()
        }
        datepicker.setOnClickListener {
            dpd.show()
        }
        timepicker.setOnClickListener {
            tpd.show()
        }
        button2.setOnClickListener {
            cd.show()
        }
    }

    override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
        message.setText("Date is : $p3 / ${p2+1} / $p1")
    }

    override fun onTimeSet(p0: TimePicker?, p1: Int, p2: Int) {
        message.setText("Time is $p1 : $p2")
    }
}